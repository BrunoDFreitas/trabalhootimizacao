param S integer;
param N integer;

set numbers := 1..N;
set sets := 1..S;
param numberInSet{sets, numbers} default 0;

var setInSol{sets} binary;

maximize z: sum{i in sets} setInSol[i];

s.t. sol{j in numbers}: sum{i in sets} setInSol[i]*numberInSet[i, j] <= 1;

end;

