JAVAC=/usr/bin/javac
JAR=/usr/bin/jar

BINDIR=bin

FILES_JAVA = src/Estruturas/Conjunto.java src/Estruturas/ListaConjuntos.java src/Estruturas/Solucao.java src/main/Main.java


FILES_CLASS = Estruturas/Conjunto.class Estruturas/ListaConjuntos.class Estruturas/Solucao.class main/Main.class

RAIZ=.

NOMEJAR=vns.jar 

make: 
	mkdir $(BINDIR)
	$(JAVAC) $(FILES_JAVA) -d $(BINDIR)
	cd $(BINDIR) && $(JAR) cfe ../$(NOMEJAR) main.Main $(FILES_CLASS)

