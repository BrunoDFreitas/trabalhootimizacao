Problem:    setpacking
Rows:       101
Columns:    500 (500 integer, 500 binary)
Non-zeros:  10500
Status:     INTEGER OPTIMAL
Objective:  z = 3 (MAXimum)

   No.   Row name        Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 z                           3                             
     2 sol[1]                      1                           1 
     3 sol[2]                      1                           1 
     4 sol[3]                      0                           1 
     5 sol[4]                      1                           1 
     6 sol[5]                      0                           1 
     7 sol[6]                      1                           1 
     8 sol[7]                      0                           1 
     9 sol[8]                      0                           1 
    10 sol[9]                      1                           1 
    11 sol[10]                     1                           1 
    12 sol[11]                     1                           1 
    13 sol[12]                     0                           1 
    14 sol[13]                     1                           1 
    15 sol[14]                     1                           1 
    16 sol[15]                     0                           1 
    17 sol[16]                     1                           1 
    18 sol[17]                     1                           1 
    19 sol[18]                     1                           1 
    20 sol[19]                     0                           1 
    21 sol[20]                     1                           1 
    22 sol[21]                     0                           1 
    23 sol[22]                     1                           1 
    24 sol[23]                     1                           1 
    25 sol[24]                     1                           1 
    26 sol[25]                     1                           1 
    27 sol[26]                     1                           1 
    28 sol[27]                     1                           1 
    29 sol[28]                     1                           1 
    30 sol[29]                     0                           1 
    31 sol[30]                     1                           1 
    32 sol[31]                     0                           1 
    33 sol[32]                     0                           1 
    34 sol[33]                     1                           1 
    35 sol[34]                     0                           1 
    36 sol[35]                     0                           1 
    37 sol[36]                     0                           1 
    38 sol[37]                     0                           1 
    39 sol[38]                     0                           1 
    40 sol[39]                     0                           1 
    41 sol[40]                     1                           1 
    42 sol[41]                     1                           1 
    43 sol[42]                     1                           1 
    44 sol[43]                     0                           1 
    45 sol[44]                     0                           1 
    46 sol[45]                     0                           1 
    47 sol[46]                     1                           1 
    48 sol[47]                     1                           1 
    49 sol[48]                     0                           1 
    50 sol[49]                     1                           1 
    51 sol[50]                     1                           1 
    52 sol[51]                     0                           1 
    53 sol[52]                     1                           1 
    54 sol[53]                     0                           1 
    55 sol[54]                     0                           1 
    56 sol[55]                     0                           1 
    57 sol[56]                     0                           1 
    58 sol[57]                     1                           1 
    59 sol[58]                     1                           1 
    60 sol[59]                     1                           1 
    61 sol[60]                     1                           1 
    62 sol[61]                     1                           1 
    63 sol[62]                     0                           1 
    64 sol[63]                     1                           1 
    65 sol[64]                     0                           1 
    66 sol[65]                     1                           1 
    67 sol[66]                     1                           1 
    68 sol[67]                     1                           1 
    69 sol[68]                     0                           1 
    70 sol[69]                     1                           1 
    71 sol[70]                     0                           1 
    72 sol[71]                     0                           1 
    73 sol[72]                     1                           1 
    74 sol[73]                     1                           1 
    75 sol[74]                     0                           1 
    76 sol[75]                     0                           1 
    77 sol[76]                     1                           1 
    78 sol[77]                     1                           1 
    79 sol[78]                     1                           1 
    80 sol[79]                     0                           1 
    81 sol[80]                     1                           1 
    82 sol[81]                     1                           1 
    83 sol[82]                     0                           1 
    84 sol[83]                     1                           1 
    85 sol[84]                     0                           1 
    86 sol[85]                     1                           1 
    87 sol[86]                     0                           1 
    88 sol[87]                     1                           1 
    89 sol[88]                     1                           1 
    90 sol[89]                     1                           1 
    91 sol[90]                     0                           1 
    92 sol[91]                     1                           1 
    93 sol[92]                     1                           1 
    94 sol[93]                     1                           1 
    95 sol[94]                     1                           1 
    96 sol[95]                     0                           1 
    97 sol[96]                     1                           1 
    98 sol[97]                     0                           1 
    99 sol[98]                     1                           1 
   100 sol[99]                     1                           1 
   101 sol[100]                    1                           1 

   No. Column name       Activity     Lower bound   Upper bound
------ ------------    ------------- ------------- -------------
     1 setInSol[1]  *              0             0             1 
     2 setInSol[2]  *              0             0             1 
     3 setInSol[3]  *              0             0             1 
     4 setInSol[4]  *              0             0             1 
     5 setInSol[5]  *              0             0             1 
     6 setInSol[6]  *              0             0             1 
     7 setInSol[7]  *              0             0             1 
     8 setInSol[8]  *              0             0             1 
     9 setInSol[9]  *              0             0             1 
    10 setInSol[10] *              0             0             1 
    11 setInSol[11] *              0             0             1 
    12 setInSol[12] *              0             0             1 
    13 setInSol[13] *              0             0             1 
    14 setInSol[14] *              0             0             1 
    15 setInSol[15] *              0             0             1 
    16 setInSol[16] *              0             0             1 
    17 setInSol[17] *              0             0             1 
    18 setInSol[18] *              0             0             1 
    19 setInSol[19] *              0             0             1 
    20 setInSol[20] *              0             0             1 
    21 setInSol[21] *              0             0             1 
    22 setInSol[22] *              0             0             1 
    23 setInSol[23] *              0             0             1 
    24 setInSol[24] *              0             0             1 
    25 setInSol[25] *              0             0             1 
    26 setInSol[26] *              0             0             1 
    27 setInSol[27] *              0             0             1 
    28 setInSol[28] *              0             0             1 
    29 setInSol[29] *              0             0             1 
    30 setInSol[30] *              0             0             1 
    31 setInSol[31] *              0             0             1 
    32 setInSol[32] *              0             0             1 
    33 setInSol[33] *              0             0             1 
    34 setInSol[34] *              0             0             1 
    35 setInSol[35] *              0             0             1 
    36 setInSol[36] *              0             0             1 
    37 setInSol[37] *              0             0             1 
    38 setInSol[38] *              0             0             1 
    39 setInSol[39] *              0             0             1 
    40 setInSol[40] *              0             0             1 
    41 setInSol[41] *              0             0             1 
    42 setInSol[42] *              0             0             1 
    43 setInSol[43] *              0             0             1 
    44 setInSol[44] *              0             0             1 
    45 setInSol[45] *              0             0             1 
    46 setInSol[46] *              0             0             1 
    47 setInSol[47] *              0             0             1 
    48 setInSol[48] *              0             0             1 
    49 setInSol[49] *              0             0             1 
    50 setInSol[50] *              0             0             1 
    51 setInSol[51] *              0             0             1 
    52 setInSol[52] *              0             0             1 
    53 setInSol[53] *              0             0             1 
    54 setInSol[54] *              0             0             1 
    55 setInSol[55] *              0             0             1 
    56 setInSol[56] *              0             0             1 
    57 setInSol[57] *              0             0             1 
    58 setInSol[58] *              0             0             1 
    59 setInSol[59] *              0             0             1 
    60 setInSol[60] *              0             0             1 
    61 setInSol[61] *              0             0             1 
    62 setInSol[62] *              0             0             1 
    63 setInSol[63] *              0             0             1 
    64 setInSol[64] *              0             0             1 
    65 setInSol[65] *              0             0             1 
    66 setInSol[66] *              0             0             1 
    67 setInSol[67] *              0             0             1 
    68 setInSol[68] *              0             0             1 
    69 setInSol[69] *              0             0             1 
    70 setInSol[70] *              0             0             1 
    71 setInSol[71] *              0             0             1 
    72 setInSol[72] *              0             0             1 
    73 setInSol[73] *              0             0             1 
    74 setInSol[74] *              0             0             1 
    75 setInSol[75] *              0             0             1 
    76 setInSol[76] *              0             0             1 
    77 setInSol[77] *              0             0             1 
    78 setInSol[78] *              0             0             1 
    79 setInSol[79] *              0             0             1 
    80 setInSol[80] *              0             0             1 
    81 setInSol[81] *              0             0             1 
    82 setInSol[82] *              0             0             1 
    83 setInSol[83] *              0             0             1 
    84 setInSol[84] *              0             0             1 
    85 setInSol[85] *              0             0             1 
    86 setInSol[86] *              0             0             1 
    87 setInSol[87] *              0             0             1 
    88 setInSol[88] *              0             0             1 
    89 setInSol[89] *              0             0             1 
    90 setInSol[90] *              0             0             1 
    91 setInSol[91] *              0             0             1 
    92 setInSol[92] *              0             0             1 
    93 setInSol[93] *              0             0             1 
    94 setInSol[94] *              0             0             1 
    95 setInSol[95] *              0             0             1 
    96 setInSol[96] *              0             0             1 
    97 setInSol[97] *              0             0             1 
    98 setInSol[98] *              0             0             1 
    99 setInSol[99] *              0             0             1 
   100 setInSol[100]
                    *              0             0             1 
   101 setInSol[101]
                    *              0             0             1 
   102 setInSol[102]
                    *              0             0             1 
   103 setInSol[103]
                    *              0             0             1 
   104 setInSol[104]
                    *              0             0             1 
   105 setInSol[105]
                    *              0             0             1 
   106 setInSol[106]
                    *              0             0             1 
   107 setInSol[107]
                    *              0             0             1 
   108 setInSol[108]
                    *              0             0             1 
   109 setInSol[109]
                    *              0             0             1 
   110 setInSol[110]
                    *              0             0             1 
   111 setInSol[111]
                    *              0             0             1 
   112 setInSol[112]
                    *              0             0             1 
   113 setInSol[113]
                    *              0             0             1 
   114 setInSol[114]
                    *              0             0             1 
   115 setInSol[115]
                    *              0             0             1 
   116 setInSol[116]
                    *              0             0             1 
   117 setInSol[117]
                    *              0             0             1 
   118 setInSol[118]
                    *              0             0             1 
   119 setInSol[119]
                    *              0             0             1 
   120 setInSol[120]
                    *              0             0             1 
   121 setInSol[121]
                    *              0             0             1 
   122 setInSol[122]
                    *              0             0             1 
   123 setInSol[123]
                    *              0             0             1 
   124 setInSol[124]
                    *              0             0             1 
   125 setInSol[125]
                    *              0             0             1 
   126 setInSol[126]
                    *              0             0             1 
   127 setInSol[127]
                    *              0             0             1 
   128 setInSol[128]
                    *              0             0             1 
   129 setInSol[129]
                    *              0             0             1 
   130 setInSol[130]
                    *              0             0             1 
   131 setInSol[131]
                    *              0             0             1 
   132 setInSol[132]
                    *              0             0             1 
   133 setInSol[133]
                    *              0             0             1 
   134 setInSol[134]
                    *              0             0             1 
   135 setInSol[135]
                    *              0             0             1 
   136 setInSol[136]
                    *              0             0             1 
   137 setInSol[137]
                    *              0             0             1 
   138 setInSol[138]
                    *              0             0             1 
   139 setInSol[139]
                    *              0             0             1 
   140 setInSol[140]
                    *              0             0             1 
   141 setInSol[141]
                    *              0             0             1 
   142 setInSol[142]
                    *              0             0             1 
   143 setInSol[143]
                    *              0             0             1 
   144 setInSol[144]
                    *              0             0             1 
   145 setInSol[145]
                    *              0             0             1 
   146 setInSol[146]
                    *              0             0             1 
   147 setInSol[147]
                    *              0             0             1 
   148 setInSol[148]
                    *              1             0             1 
   149 setInSol[149]
                    *              0             0             1 
   150 setInSol[150]
                    *              0             0             1 
   151 setInSol[151]
                    *              0             0             1 
   152 setInSol[152]
                    *              0             0             1 
   153 setInSol[153]
                    *              0             0             1 
   154 setInSol[154]
                    *              0             0             1 
   155 setInSol[155]
                    *              0             0             1 
   156 setInSol[156]
                    *              0             0             1 
   157 setInSol[157]
                    *              0             0             1 
   158 setInSol[158]
                    *              0             0             1 
   159 setInSol[159]
                    *              0             0             1 
   160 setInSol[160]
                    *              0             0             1 
   161 setInSol[161]
                    *              0             0             1 
   162 setInSol[162]
                    *              0             0             1 
   163 setInSol[163]
                    *              0             0             1 
   164 setInSol[164]
                    *              0             0             1 
   165 setInSol[165]
                    *              0             0             1 
   166 setInSol[166]
                    *              0             0             1 
   167 setInSol[167]
                    *              0             0             1 
   168 setInSol[168]
                    *              0             0             1 
   169 setInSol[169]
                    *              0             0             1 
   170 setInSol[170]
                    *              0             0             1 
   171 setInSol[171]
                    *              0             0             1 
   172 setInSol[172]
                    *              0             0             1 
   173 setInSol[173]
                    *              0             0             1 
   174 setInSol[174]
                    *              0             0             1 
   175 setInSol[175]
                    *              0             0             1 
   176 setInSol[176]
                    *              0             0             1 
   177 setInSol[177]
                    *              0             0             1 
   178 setInSol[178]
                    *              0             0             1 
   179 setInSol[179]
                    *              0             0             1 
   180 setInSol[180]
                    *              0             0             1 
   181 setInSol[181]
                    *              0             0             1 
   182 setInSol[182]
                    *              0             0             1 
   183 setInSol[183]
                    *              0             0             1 
   184 setInSol[184]
                    *              0             0             1 
   185 setInSol[185]
                    *              0             0             1 
   186 setInSol[186]
                    *              0             0             1 
   187 setInSol[187]
                    *              0             0             1 
   188 setInSol[188]
                    *              0             0             1 
   189 setInSol[189]
                    *              0             0             1 
   190 setInSol[190]
                    *              0             0             1 
   191 setInSol[191]
                    *              0             0             1 
   192 setInSol[192]
                    *              0             0             1 
   193 setInSol[193]
                    *              0             0             1 
   194 setInSol[194]
                    *              0             0             1 
   195 setInSol[195]
                    *              0             0             1 
   196 setInSol[196]
                    *              0             0             1 
   197 setInSol[197]
                    *              0             0             1 
   198 setInSol[198]
                    *              0             0             1 
   199 setInSol[199]
                    *              0             0             1 
   200 setInSol[200]
                    *              0             0             1 
   201 setInSol[201]
                    *              0             0             1 
   202 setInSol[202]
                    *              0             0             1 
   203 setInSol[203]
                    *              0             0             1 
   204 setInSol[204]
                    *              0             0             1 
   205 setInSol[205]
                    *              0             0             1 
   206 setInSol[206]
                    *              0             0             1 
   207 setInSol[207]
                    *              0             0             1 
   208 setInSol[208]
                    *              0             0             1 
   209 setInSol[209]
                    *              0             0             1 
   210 setInSol[210]
                    *              0             0             1 
   211 setInSol[211]
                    *              0             0             1 
   212 setInSol[212]
                    *              0             0             1 
   213 setInSol[213]
                    *              0             0             1 
   214 setInSol[214]
                    *              0             0             1 
   215 setInSol[215]
                    *              0             0             1 
   216 setInSol[216]
                    *              0             0             1 
   217 setInSol[217]
                    *              0             0             1 
   218 setInSol[218]
                    *              0             0             1 
   219 setInSol[219]
                    *              0             0             1 
   220 setInSol[220]
                    *              0             0             1 
   221 setInSol[221]
                    *              0             0             1 
   222 setInSol[222]
                    *              0             0             1 
   223 setInSol[223]
                    *              0             0             1 
   224 setInSol[224]
                    *              0             0             1 
   225 setInSol[225]
                    *              0             0             1 
   226 setInSol[226]
                    *              0             0             1 
   227 setInSol[227]
                    *              0             0             1 
   228 setInSol[228]
                    *              0             0             1 
   229 setInSol[229]
                    *              0             0             1 
   230 setInSol[230]
                    *              0             0             1 
   231 setInSol[231]
                    *              0             0             1 
   232 setInSol[232]
                    *              0             0             1 
   233 setInSol[233]
                    *              0             0             1 
   234 setInSol[234]
                    *              0             0             1 
   235 setInSol[235]
                    *              0             0             1 
   236 setInSol[236]
                    *              0             0             1 
   237 setInSol[237]
                    *              0             0             1 
   238 setInSol[238]
                    *              0             0             1 
   239 setInSol[239]
                    *              0             0             1 
   240 setInSol[240]
                    *              0             0             1 
   241 setInSol[241]
                    *              0             0             1 
   242 setInSol[242]
                    *              0             0             1 
   243 setInSol[243]
                    *              0             0             1 
   244 setInSol[244]
                    *              0             0             1 
   245 setInSol[245]
                    *              0             0             1 
   246 setInSol[246]
                    *              0             0             1 
   247 setInSol[247]
                    *              0             0             1 
   248 setInSol[248]
                    *              1             0             1 
   249 setInSol[249]
                    *              0             0             1 
   250 setInSol[250]
                    *              0             0             1 
   251 setInSol[251]
                    *              0             0             1 
   252 setInSol[252]
                    *              0             0             1 
   253 setInSol[253]
                    *              0             0             1 
   254 setInSol[254]
                    *              0             0             1 
   255 setInSol[255]
                    *              0             0             1 
   256 setInSol[256]
                    *              0             0             1 
   257 setInSol[257]
                    *              0             0             1 
   258 setInSol[258]
                    *              0             0             1 
   259 setInSol[259]
                    *              0             0             1 
   260 setInSol[260]
                    *              0             0             1 
   261 setInSol[261]
                    *              0             0             1 
   262 setInSol[262]
                    *              0             0             1 
   263 setInSol[263]
                    *              0             0             1 
   264 setInSol[264]
                    *              0             0             1 
   265 setInSol[265]
                    *              0             0             1 
   266 setInSol[266]
                    *              0             0             1 
   267 setInSol[267]
                    *              0             0             1 
   268 setInSol[268]
                    *              0             0             1 
   269 setInSol[269]
                    *              0             0             1 
   270 setInSol[270]
                    *              0             0             1 
   271 setInSol[271]
                    *              0             0             1 
   272 setInSol[272]
                    *              0             0             1 
   273 setInSol[273]
                    *              0             0             1 
   274 setInSol[274]
                    *              0             0             1 
   275 setInSol[275]
                    *              0             0             1 
   276 setInSol[276]
                    *              0             0             1 
   277 setInSol[277]
                    *              0             0             1 
   278 setInSol[278]
                    *              0             0             1 
   279 setInSol[279]
                    *              0             0             1 
   280 setInSol[280]
                    *              0             0             1 
   281 setInSol[281]
                    *              0             0             1 
   282 setInSol[282]
                    *              0             0             1 
   283 setInSol[283]
                    *              0             0             1 
   284 setInSol[284]
                    *              0             0             1 
   285 setInSol[285]
                    *              0             0             1 
   286 setInSol[286]
                    *              0             0             1 
   287 setInSol[287]
                    *              0             0             1 
   288 setInSol[288]
                    *              0             0             1 
   289 setInSol[289]
                    *              0             0             1 
   290 setInSol[290]
                    *              0             0             1 
   291 setInSol[291]
                    *              0             0             1 
   292 setInSol[292]
                    *              0             0             1 
   293 setInSol[293]
                    *              0             0             1 
   294 setInSol[294]
                    *              0             0             1 
   295 setInSol[295]
                    *              0             0             1 
   296 setInSol[296]
                    *              0             0             1 
   297 setInSol[297]
                    *              0             0             1 
   298 setInSol[298]
                    *              0             0             1 
   299 setInSol[299]
                    *              0             0             1 
   300 setInSol[300]
                    *              0             0             1 
   301 setInSol[301]
                    *              0             0             1 
   302 setInSol[302]
                    *              0             0             1 
   303 setInSol[303]
                    *              0             0             1 
   304 setInSol[304]
                    *              0             0             1 
   305 setInSol[305]
                    *              0             0             1 
   306 setInSol[306]
                    *              0             0             1 
   307 setInSol[307]
                    *              0             0             1 
   308 setInSol[308]
                    *              0             0             1 
   309 setInSol[309]
                    *              0             0             1 
   310 setInSol[310]
                    *              0             0             1 
   311 setInSol[311]
                    *              0             0             1 
   312 setInSol[312]
                    *              0             0             1 
   313 setInSol[313]
                    *              0             0             1 
   314 setInSol[314]
                    *              0             0             1 
   315 setInSol[315]
                    *              0             0             1 
   316 setInSol[316]
                    *              0             0             1 
   317 setInSol[317]
                    *              0             0             1 
   318 setInSol[318]
                    *              0             0             1 
   319 setInSol[319]
                    *              0             0             1 
   320 setInSol[320]
                    *              0             0             1 
   321 setInSol[321]
                    *              0             0             1 
   322 setInSol[322]
                    *              0             0             1 
   323 setInSol[323]
                    *              0             0             1 
   324 setInSol[324]
                    *              0             0             1 
   325 setInSol[325]
                    *              0             0             1 
   326 setInSol[326]
                    *              0             0             1 
   327 setInSol[327]
                    *              0             0             1 
   328 setInSol[328]
                    *              0             0             1 
   329 setInSol[329]
                    *              0             0             1 
   330 setInSol[330]
                    *              0             0             1 
   331 setInSol[331]
                    *              0             0             1 
   332 setInSol[332]
                    *              0             0             1 
   333 setInSol[333]
                    *              0             0             1 
   334 setInSol[334]
                    *              0             0             1 
   335 setInSol[335]
                    *              0             0             1 
   336 setInSol[336]
                    *              0             0             1 
   337 setInSol[337]
                    *              0             0             1 
   338 setInSol[338]
                    *              0             0             1 
   339 setInSol[339]
                    *              0             0             1 
   340 setInSol[340]
                    *              0             0             1 
   341 setInSol[341]
                    *              0             0             1 
   342 setInSol[342]
                    *              0             0             1 
   343 setInSol[343]
                    *              0             0             1 
   344 setInSol[344]
                    *              0             0             1 
   345 setInSol[345]
                    *              0             0             1 
   346 setInSol[346]
                    *              0             0             1 
   347 setInSol[347]
                    *              0             0             1 
   348 setInSol[348]
                    *              0             0             1 
   349 setInSol[349]
                    *              0             0             1 
   350 setInSol[350]
                    *              0             0             1 
   351 setInSol[351]
                    *              0             0             1 
   352 setInSol[352]
                    *              0             0             1 
   353 setInSol[353]
                    *              0             0             1 
   354 setInSol[354]
                    *              0             0             1 
   355 setInSol[355]
                    *              0             0             1 
   356 setInSol[356]
                    *              0             0             1 
   357 setInSol[357]
                    *              0             0             1 
   358 setInSol[358]
                    *              0             0             1 
   359 setInSol[359]
                    *              0             0             1 
   360 setInSol[360]
                    *              0             0             1 
   361 setInSol[361]
                    *              0             0             1 
   362 setInSol[362]
                    *              0             0             1 
   363 setInSol[363]
                    *              0             0             1 
   364 setInSol[364]
                    *              0             0             1 
   365 setInSol[365]
                    *              0             0             1 
   366 setInSol[366]
                    *              0             0             1 
   367 setInSol[367]
                    *              0             0             1 
   368 setInSol[368]
                    *              0             0             1 
   369 setInSol[369]
                    *              0             0             1 
   370 setInSol[370]
                    *              0             0             1 
   371 setInSol[371]
                    *              0             0             1 
   372 setInSol[372]
                    *              0             0             1 
   373 setInSol[373]
                    *              0             0             1 
   374 setInSol[374]
                    *              0             0             1 
   375 setInSol[375]
                    *              0             0             1 
   376 setInSol[376]
                    *              0             0             1 
   377 setInSol[377]
                    *              0             0             1 
   378 setInSol[378]
                    *              0             0             1 
   379 setInSol[379]
                    *              0             0             1 
   380 setInSol[380]
                    *              0             0             1 
   381 setInSol[381]
                    *              0             0             1 
   382 setInSol[382]
                    *              0             0             1 
   383 setInSol[383]
                    *              0             0             1 
   384 setInSol[384]
                    *              0             0             1 
   385 setInSol[385]
                    *              0             0             1 
   386 setInSol[386]
                    *              0             0             1 
   387 setInSol[387]
                    *              0             0             1 
   388 setInSol[388]
                    *              0             0             1 
   389 setInSol[389]
                    *              0             0             1 
   390 setInSol[390]
                    *              0             0             1 
   391 setInSol[391]
                    *              0             0             1 
   392 setInSol[392]
                    *              0             0             1 
   393 setInSol[393]
                    *              0             0             1 
   394 setInSol[394]
                    *              0             0             1 
   395 setInSol[395]
                    *              1             0             1 
   396 setInSol[396]
                    *              0             0             1 
   397 setInSol[397]
                    *              0             0             1 
   398 setInSol[398]
                    *              0             0             1 
   399 setInSol[399]
                    *              0             0             1 
   400 setInSol[400]
                    *              0             0             1 
   401 setInSol[401]
                    *              0             0             1 
   402 setInSol[402]
                    *              0             0             1 
   403 setInSol[403]
                    *              0             0             1 
   404 setInSol[404]
                    *              0             0             1 
   405 setInSol[405]
                    *              0             0             1 
   406 setInSol[406]
                    *              0             0             1 
   407 setInSol[407]
                    *              0             0             1 
   408 setInSol[408]
                    *              0             0             1 
   409 setInSol[409]
                    *              0             0             1 
   410 setInSol[410]
                    *              0             0             1 
   411 setInSol[411]
                    *              0             0             1 
   412 setInSol[412]
                    *              0             0             1 
   413 setInSol[413]
                    *              0             0             1 
   414 setInSol[414]
                    *              0             0             1 
   415 setInSol[415]
                    *              0             0             1 
   416 setInSol[416]
                    *              0             0             1 
   417 setInSol[417]
                    *              0             0             1 
   418 setInSol[418]
                    *              0             0             1 
   419 setInSol[419]
                    *              0             0             1 
   420 setInSol[420]
                    *              0             0             1 
   421 setInSol[421]
                    *              0             0             1 
   422 setInSol[422]
                    *              0             0             1 
   423 setInSol[423]
                    *              0             0             1 
   424 setInSol[424]
                    *              0             0             1 
   425 setInSol[425]
                    *              0             0             1 
   426 setInSol[426]
                    *              0             0             1 
   427 setInSol[427]
                    *              0             0             1 
   428 setInSol[428]
                    *              0             0             1 
   429 setInSol[429]
                    *              0             0             1 
   430 setInSol[430]
                    *              0             0             1 
   431 setInSol[431]
                    *              0             0             1 
   432 setInSol[432]
                    *              0             0             1 
   433 setInSol[433]
                    *              0             0             1 
   434 setInSol[434]
                    *              0             0             1 
   435 setInSol[435]
                    *              0             0             1 
   436 setInSol[436]
                    *              0             0             1 
   437 setInSol[437]
                    *              0             0             1 
   438 setInSol[438]
                    *              0             0             1 
   439 setInSol[439]
                    *              0             0             1 
   440 setInSol[440]
                    *              0             0             1 
   441 setInSol[441]
                    *              0             0             1 
   442 setInSol[442]
                    *              0             0             1 
   443 setInSol[443]
                    *              0             0             1 
   444 setInSol[444]
                    *              0             0             1 
   445 setInSol[445]
                    *              0             0             1 
   446 setInSol[446]
                    *              0             0             1 
   447 setInSol[447]
                    *              0             0             1 
   448 setInSol[448]
                    *              0             0             1 
   449 setInSol[449]
                    *              0             0             1 
   450 setInSol[450]
                    *              0             0             1 
   451 setInSol[451]
                    *              0             0             1 
   452 setInSol[452]
                    *              0             0             1 
   453 setInSol[453]
                    *              0             0             1 
   454 setInSol[454]
                    *              0             0             1 
   455 setInSol[455]
                    *              0             0             1 
   456 setInSol[456]
                    *              0             0             1 
   457 setInSol[457]
                    *              0             0             1 
   458 setInSol[458]
                    *              0             0             1 
   459 setInSol[459]
                    *              0             0             1 
   460 setInSol[460]
                    *              0             0             1 
   461 setInSol[461]
                    *              0             0             1 
   462 setInSol[462]
                    *              0             0             1 
   463 setInSol[463]
                    *              0             0             1 
   464 setInSol[464]
                    *              0             0             1 
   465 setInSol[465]
                    *              0             0             1 
   466 setInSol[466]
                    *              0             0             1 
   467 setInSol[467]
                    *              0             0             1 
   468 setInSol[468]
                    *              0             0             1 
   469 setInSol[469]
                    *              0             0             1 
   470 setInSol[470]
                    *              0             0             1 
   471 setInSol[471]
                    *              0             0             1 
   472 setInSol[472]
                    *              0             0             1 
   473 setInSol[473]
                    *              0             0             1 
   474 setInSol[474]
                    *              0             0             1 
   475 setInSol[475]
                    *              0             0             1 
   476 setInSol[476]
                    *              0             0             1 
   477 setInSol[477]
                    *              0             0             1 
   478 setInSol[478]
                    *              0             0             1 
   479 setInSol[479]
                    *              0             0             1 
   480 setInSol[480]
                    *              0             0             1 
   481 setInSol[481]
                    *              0             0             1 
   482 setInSol[482]
                    *              0             0             1 
   483 setInSol[483]
                    *              0             0             1 
   484 setInSol[484]
                    *              0             0             1 
   485 setInSol[485]
                    *              0             0             1 
   486 setInSol[486]
                    *              0             0             1 
   487 setInSol[487]
                    *              0             0             1 
   488 setInSol[488]
                    *              0             0             1 
   489 setInSol[489]
                    *              0             0             1 
   490 setInSol[490]
                    *              0             0             1 
   491 setInSol[491]
                    *              0             0             1 
   492 setInSol[492]
                    *              0             0             1 
   493 setInSol[493]
                    *              0             0             1 
   494 setInSol[494]
                    *              0             0             1 
   495 setInSol[495]
                    *              0             0             1 
   496 setInSol[496]
                    *              0             0             1 
   497 setInSol[497]
                    *              0             0             1 
   498 setInSol[498]
                    *              0             0             1 
   499 setInSol[499]
                    *              0             0             1 
   500 setInSol[500]
                    *              0             0             1 

Integer feasibility conditions:

KKT.PE: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

KKT.PB: max.abs.err = 0.00e+00 on row 0
        max.rel.err = 0.00e+00 on row 0
        High quality

End of output
