package main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Random;

import Estruturas.ListaConjuntos;
import Estruturas.Solucao;

public class Main {
	
	private static String caminhoInstancias = "instancias\\";
	private static String caminhoResultados = "resultados\\";

	public static void main(String[] args) {
		int maxK = 0;
		long tempoMax = 0;		
		boolean imprimirItens = false;
		
		String nomeArquivo = null;
		
		for(int i = 0; i < args.length; i++){
			if(args[i].compareTo("-t") == 0){
				i++;
				tempoMax = Long.parseLong(args[i])*(60000L);
			}
			
			if(args[i].compareTo("-k") == 0){
				i++;
				maxK = Integer.parseInt(args[i]);
			}
			
			if(args[i].compareTo("-a") == 0){
				i++;
				nomeArquivo = args[i];
			}
			
			if(args[i].compareTo("-i") == 0){
				imprimirItens = true;
			}
		}
		
		
		if(nomeArquivo == null){
			System.out.println("O nome do arquivo deve ser informado utilizando o parâmetro '-a'!");
			return;
		}
		
		
		if(tempoMax <= 0){
			System.out.println("O tempo máximo de execução deve ser positivo e deve ser informado utilizando o parâmetro -t!");
			return;
		}
		
		
		if(tempoMax <= 0){
			System.out.println("O tempo máximo de execução, em minutos, deve ser positivo e é informado utilizando o parâmetro -t!");
			return;
		}
		
		if(maxK <= 0){
			System.out.println("A vizinhança mais distante a ser explorada deve ser positivo e é informado utilizando o parâmetro -k!");
			return;
		}
		
		File arquivoEntrada = new File(caminhoInstancias + nomeArquivo);
		if(!arquivoEntrada.exists()){
			arquivoEntrada = new File(nomeArquivo);
			if(!arquivoEntrada.exists()){
				System.out.println("Arquivo " + nomeArquivo + " não encontrado na pasta raiz nem na pasta " + caminhoInstancias + "!");
				return;
			}
		}
			
		ListaConjuntos lstConj;
		Solucao solucao;
		Solucao novaSolucao;

		// Prepara o arquivo de saída
		int contador = 1;
		File arquivoSaida = new File(caminhoResultados + nomeArquivo + contador + ".txt");
		while(arquivoSaida.exists()){
			contador++;
			arquivoSaida = new File(caminhoResultados + nomeArquivo + contador + ".txt");
		}
		
		FileWriter arquivoWrite;
		try {
			arquivoWrite = new FileWriter(arquivoSaida);
		} catch (IOException e1) {
			System.out.println("Erro ao criar o arquivo de saída!");
			return;
		}
		PrintWriter gravarArq = new PrintWriter(arquivoWrite);
		// Fim da preparação do arquivo de saída
		
		// Contagem do tempo de execução da heurística
		long inicio = System.currentTimeMillis();
		
		lstConj = new ListaConjuntos(arquivoEntrada);		
		
		solucao = new Solucao();
		solucao.setLstConj(lstConj);
		solucao.setTempoMax(tempoMax);
		
		/** Geração da solução inicial **/
		solucao.geraSolucaoInicial();
		long tempoMelhorSol = System.currentTimeMillis() - inicio;
		System.out.println("***** SOLUÇÃO INICIAL *****");
		gravarArq.println("***** SOLUÇÃO INICIAL *****");
		solucao.imprimeSolucao(gravarArq, false,true);
		
		
		solucao.setTempoInicio(inicio);
		try{
			do{
				int k = 1;
				while(k <= maxK){
					novaSolucao = solucao.geraSolucaoRandomica(k);
					
					novaSolucao.buscaLocal();
					
					// Encontrado uma solução melhor do que a anterior
					if(solucao.size() < novaSolucao.size()){
						solucao = novaSolucao;
						tempoMelhorSol = System.currentTimeMillis() - inicio;
						
						System.out.println("ENCONTRADA UMA MELHOR SOLUÇÃO");
						System.out.println("Tempo execução: " + (tempoMelhorSol/1000L) + " segundos");
						
						gravarArq.println("ENCONTRADA UMA MELHOR SOLUÇÃO");
						gravarArq.println("Tempo execução: " + (tempoMelhorSol/1000L) + " segundos");
						solucao.imprimeSolucao(gravarArq,false,true);
						
						k = 1;
					}
					// Caso empate escolhe uma solução aleatória
					else {
						k++;
						if(solucao.size() == novaSolucao.size()){
							Random random = new Random();
							if(random.nextInt(2) == 0)
								solucao = novaSolucao;
						}
					}
				}
				
			} while(true);
		}catch (Exception e) {
			System.out.println("***** FIM DO TEMPO DE EXECUÇÃO *****\n");
		}
		System.out.println("***** RESULTADO *****");
		System.out.println("Tempo para encontrar a melhor solução: " + ((tempoMelhorSol)/1000L) + " segundos");
		System.out.println("Tempo de execução total: " + ((System.currentTimeMillis() - inicio)/1000L) + " segundos");
		
		gravarArq.println("***** RESULTADO *****");
		gravarArq.println("Tempo para encontrar a melhor solução: " + ((tempoMelhorSol)/1000L) + " segundos");
		gravarArq.println("Tempo de execução total: " + ((System.currentTimeMillis() - inicio)/1000L) + " segundos");
		
		Collections.sort(solucao);
		solucao.imprimeSolucao(gravarArq, imprimirItens,true);
		
		try {
			arquivoWrite.close();
		} catch (IOException e) {
			System.out.println("Erro ao fechar o arquivo de resultado!");
			return;
		}
		
		System.out.println("\nResultado gravado em: " + arquivoSaida.getAbsolutePath());
	}
}
