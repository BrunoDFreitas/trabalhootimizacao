package Estruturas;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Solucao extends ArrayList<Integer>{
	
	private static final long serialVersionUID = 1L;
	
//	private int qtdItens;
	private ListaConjuntos lstConj;
	private List<List<Integer>> lstAdj; 
	private long tempoMax;
	private long tempoInicio;
	
	
	public Solucao(){
		super();
//		qtdItens = 0;
		tempoMax = 0;
		tempoInicio = 0;
	}
	
	
	public Solucao(Solucao s){
		this();
		
		for(int i = 0; i < s.size(); i++){
			this.add(s.get(i));
		}
		lstConj = s.getLstConj();
		lstAdj = s.getLstAdj();
//		qtdItens = s.getQtdItens();
		tempoMax = s.getTempoMax();
		tempoInicio = s.getTempoInicio();
	}
	
	
	//********** SETs e GETs ***********//
	
	public void setLstConj(ListaConjuntos lstConj) {
		this.lstConj = lstConj;
		lstAdj = lstConj.geraListaAdj();
	}
	
	public ListaConjuntos getLstConj() {
		return lstConj;
	}
	
	
	public void setTempoMax(long tempoMax) {
		this.tempoMax = tempoMax;
	}
	
	
	public long getTempoMax() {
		return tempoMax;
	}
	
	
	public void setTempoInicio(long tempoInicio) {
		this.tempoInicio = tempoInicio;
	}
	
	
	public long getTempoInicio() {
		return tempoInicio;
	}
	
	
	public void setLstAdj(List<List<Integer>> lstAdj) {
		this.lstAdj = lstAdj;
	}
	
	public List<List<Integer>> getLstAdj() {
		return lstAdj;
	}
	
	//********** SETs e GETs ***********//
	
	
	/**
	 * Seleciona uma solu��o aleat�ria da vizinhan�a k.
	 * @param k - Vizinha�a da solu��o a ser selecionada.
	 * @return solu�ao da vizinhan�a k.
	 */
	public Solucao geraSolucaoRandomica(int k){
		List<Integer> lstRestricao = new ArrayList<Integer>();
		return this.geraSolucaoRandomica(k, lstRestricao);
	}
	
	
	/**
	 * Fun��o privada utilizada para gerar uma solu��o aleat�ria da 
	 * vizinhan�a k.
	 * @param k - Par�metro que indica a quantidade de conjuntos
	 * 			da solu��o original ser�o alterados.
	 * @param lstRestricao - Lista de conjuntos que n�o poder�o entrar
	 * 			na nova solu��o. Utilizado para evitar que seja retornado
	 * 			a mesma solu��o.
	 * @return Solu��o da vizinhan�a k
	 */
	private Solucao geraSolucaoRandomica(int k, List<Integer> lstRestricao){
		// N�o � poss�vel remover mais nenhum conjunto
		if(this.size() <= 0)
			return this;
		
		// Faz uma c�pia da solu��o original
		Solucao novaSolucao = new Solucao(this);
		List<Integer> lstEntradas;
		
		// Remove um conjunto aleat�rio 
		Random random = new Random();
		int index = random.nextInt(novaSolucao.size());
		lstRestricao.add(novaSolucao.removeConj(index));
					
		// Aproveita a recursividade para gerar vizinho mais distantes
		if(k > 1){
			novaSolucao = novaSolucao.geraSolucaoRandomica(k-1, lstRestricao);
		}
		
		// Seleciona um novo conjunto para fazer para da nova solu��o
		lstEntradas = novaSolucao.selecConjuntosNaoAdj(lstRestricao);
		int idConj;
		if(lstEntradas.size() > 0){
			index = random.nextInt(lstEntradas.size());
			idConj = lstEntradas.get(index);
			novaSolucao.add(idConj);
		}
		
		return novaSolucao;
	}
	
	
	/**
	 * Fun��o utilizada para acrescentar conjuntos que n�o s�o adjacentes 
	 * aos conjuntos da solu��o.
	 * @param n - Vari�vel para controle da recursividade.
	 * @param qtdAtual - Quantidade de itens da solu��o original.
	 * @param max - Vari�vel necess�ria para chamar a fun��o buscaLocal.
	 * @param lstRestricao - Lista de conjuntos que n�o poder�o entrar
	 * 			na solu��o.
	 * @return True caso a soma de itens do acr�scimo for maior do que 
	 * 			qtdItem, false caso contr�rio. 
	 * @throws Exception - Caso expire o tempo de execu��o.
	 */
	private boolean acrescentarConj(int n, int qtdAtual, int max, List<Integer> lstRestricao) throws Exception{
		List<Integer> lstEntrada;
		int idEntra;
		
		lstEntrada = this.selecConjuntosNaoAdj(lstRestricao);
		for(int j = 0; j < lstEntrada.size(); j++){
			// Adiciona o conjunto
			idEntra = lstEntrada.get(j);			
			this.add(idEntra);
			
			// Remove um outro conjunto da solu��o original
			if(n > 1){
				if(this.buscaLocal(n-1, qtdAtual, max, lstRestricao))
					return true;
			}
			// Verifica se n�o � poss�vel adicionar mais conjuntos
			else if(n > 0){
				if(this.acrescentarConj(n-1, qtdAtual, 0, lstRestricao))
					return true;
			}
			
			// O n�mero de conjuntos � maior do que a da solu��o original
			if(this.size() > qtdAtual){
				return true;
			}
			
			// Remove o conjunto inclu�do anteriormente
			this.remove(this.size()-1);
		}
		
		return false;
	}

	
	/**
	 * Faz uma busca local utilizando como crit�rio First Improvement, ou seja,
	 * para ao achar a primeira solu��o maior do que a solu��o que chamou esta
	 * fun��o.
	 * @throws Exception - Caso expire o tempo de execu��o.
	 */
	public void buscaLocal() throws Exception{
		List<Integer> lstRestricao = new ArrayList<Integer>();
		int qtdAtual = this.size();
		this.buscaLocal(this.size(), qtdAtual, this.size(), lstRestricao);
	}
	
	
	/**
	 * Faz a busca local com First Improvement como crit�rio de parada.
	 * Aproveita recusividade para buscas profundas.
	 * @param n - quantidade de conjuntos da solu��o original que sair�o.
	 * @param qtdAtual - quantidade de conjuntos da solu��o original.
	 * @param max - controle para retirar apenas conjuntos da solu��o original.
	 * @param lstRestricao - conjuntos que n�o poder�o entrar na solu��o.
	 * 			Utilizado para evitar a entrada de conjuntos j� removidos.
	 * @return True caso a soma de conjuntos da nova solu��o for maior do que 
	 * 			qtdItem, false caso contr�rio. 
	 * @throws Exception - Caso expire o tempo de execu��o.
	 */
	private boolean buscaLocal(int n, int qtdAtual, int max, List<Integer> lstRestricao) throws Exception{
		if(System.currentTimeMillis() - tempoInicio > tempoMax){
			throw new Exception();
		}
		if(n > 1){
			// Recursividade
			if(this.buscaLocal(n-1,qtdAtual, max, lstRestricao))
				return true;
		}
		else {
			// Tenta acrescentar mais conjuntos antes de remover os conjuntos originais
			if(this.acrescentarConj(n, qtdAtual, max, lstRestricao))
				return true;
		}
		int idSai;
		
		// Troca os conjuntos por novos conjuntos
		for(int i = 0; i < max; i++){
			idSai = this.removeConj(i);
			lstRestricao.add(idSai);
			
			// Ap�s altera��o do conjunto, verifica se � poss�vel acrescentar mais conjuntos
			if(this.acrescentarConj(n, qtdAtual, max-1, lstRestricao))
				return true;
			
			// Verifica se a quantidade de conjuntos � maior do que a da solu��o inicial
			if(this.size() > qtdAtual){
				return true;
			}
			
			// Remove o conjunto acrescentado anteriormente;
			this.add(i, idSai);
			lstRestricao.remove(new Integer(idSai));
		}
			
		return false;
	}
	
	
		
	/**
	 * Remove o conjunto da posi��o index e retorna o id deste conjunto.
	 * @param index - Posi��o do conjunto na lista da solu��o.
	 * @return Id do conjunto removido.
	 */
	private int removeConj(int index){
		int idConj = this.get(index);
		this.remove(index);
		return idConj;
	}
	
	
	/**
	 * Seleciona a lista de conjuntos n�o adjacentes da solu��o
	 * excluindo o conjunto de id <b>restricao</b>.
	 * @param restricao - Id do conjunto a ser desconsiderado.
	 * @return
	 */
	private List<Integer> selecConjuntosNaoAdj(int restricao){
		List<Integer> lstRestricao = new ArrayList<Integer>();
		lstRestricao.add(restricao);
		return selecConjuntosNaoAdj(lstRestricao);
	}
	
	
	/**
	 * Seleciona a lista de conjuntos n�o adjacentes da solu��o
	 * excluindo os conjuntos pertencentes a lista de ids <b>lstRestricao</b>.
	 * @param lstRestricao - Lista de ids dos conjuntos a serem desconsiderados.
	 * @return
	 */
	private List<Integer> selecConjuntosNaoAdj(List<Integer> lstRestricao){
		List<Integer> lstResultado = new ArrayList<Integer>();

		for(int i = 0; i < lstConj.size(); i++){
			if(!conjuntoEhAdjacente(i)){
				if(!lstRestricao.contains(i) && !this.contains(i)){
					lstResultado.add(i);
				}
			}
		}
		return lstResultado;
	}	
	
	
	
	/**
	 * Verifica se o conjunto � adjacente a algum dos conjuntos
	 * que fazem parte da solu��o.
	 * @param idConj - id do conjunto.
	 * @return True caso verdadeiro, false caso contr�rio.
	 */
	private boolean conjuntoEhAdjacente(int idConj){
		int idAux;
		for(int i = 0; i < this.size(); i++){
			idAux = this.get(i);
			if(lstAdj.get(idAux).contains(idConj)){
				return true;
			}
		}
		return false;
	}	
	
	
	/**
	 * Gera uma solu��o inicial escolhendo os conjuntos com a menor quantidade
	 * de conjuntos adjacentes.
	 */
	public void geraSolucaoInicial(){
		List<Integer> lst = this.selecConjuntosNaoAdj(-1);
		List<Integer> lstId;
		int index;
		int idConj;
		Random random = new Random();
		while(lst.size() > 0){
			lstId = this.conjMenorQtdAdj(lst);
			index = random.nextInt(lstId.size());
			idConj = lstId.get(index);
			this.add(idConj);
			lst = this.selecConjuntosNaoAdj(-1);
		}		
	}
	
	
	/**
	 * Seleciona os conjuntos com a menor quantidade de conjuntos adjacentes.
	 * @param lst - lista de conjuntos a ser selecionados.
	 * @return Retorna a lista de coonjuntos.
	 */
	private List<Integer> conjMenorQtdAdj(List<Integer> lst){
		List<Integer> lstId = new ArrayList<Integer>();
		int qtd = lstConj.size();
		
		for(int i = 0; i < lst.size(); i++){
			int index = lst.get(i);
			if(lstAdj.get(index).size() < qtd){
				lstId = new ArrayList<Integer>();
				lstId.add(index);
				qtd = lstAdj.get(index).size();
			}
			else if(lstAdj.get(index).size() == qtd){
				lstId.add(index);
			}
		}
		return lstId;
	}
	
	
	/**
	 * Imprime os dados da solu��o atual. Caso <b>itens</b> seja
	 * true, imprime os itens contidos na solu��o. Se <b>gravar</b>
	 * for true tamb�m grava os dados no arquivo <b>gravarArq</b>
	 * @param gravarArq - Arquivo na qual vai ser gravados os dados.
	 * @param itens - booleano indicado se � para imprimir os itens
	 * 			contidos na solu��o.
	 * @param gravar - True caso deva gravar resultado no arquivo, 
	 * 			false caso contr�rio
	 */
	public void imprimeSolucao(PrintWriter gravarArq, boolean itens, boolean gravar){
		System.out.println("=====================================");
		System.out.println("N�mero de Conjuntos: " + this.size());
		System.out.println("Conjuntos: ");
		
		if(gravar){
			gravarArq.println("=====================================");
			gravarArq.println("N�mero de Conjuntos: " + this.size());
			gravarArq.println("Conjuntos: ");
		}
		for(int i = 0; i < this.size(); i++){
			System.out.print(this.get(i) + " ");
			if(gravar)
				gravarArq.print(this.get(i) + " ");
		}
		System.out.println();
		if(gravar)
			gravarArq.println();
		
		if(itens){
			imprimeItens(gravarArq, gravar);
		}
		System.out.println("=====================================");
		System.out.println();
		if(gravar){
			gravarArq.println("=====================================");
			gravarArq.println();
		}
	}
	
	
	/**
	 * Imprime os itens contidos na solu��o.
	 * @param gravarArq - Arquivo na qual vai ser gravados os dados.
	 * @param gravar - True caso deva gravar resultado no arquivo, 
	 * 			false caso contr�rio
	 */
	private void imprimeItens(PrintWriter gravarArq, boolean gravar){
		System.out.println("N�mero de Itens: " + this.totalItens());
		System.out.println("Itens: ");
		if(gravar){
			gravarArq.println("N�mero de Itens: " + this.totalItens());
			gravarArq.println("Itens: ");
		}
		List<Integer> lst = new ArrayList<Integer>();
		int idConj;
		for(int i = 0; i < this.size(); i++){
			idConj = this.get(i);
			lst.addAll(lstConj.get(idConj));
		}
		Collections.sort(lst);
		for(int i = 0; i < lst.size(); i++){
			System.out.print(lst.get(i) + " ");
			if(gravar)
				gravarArq.print(lst.get(i) + " ");
			if(i%100 == 99){
				System.out.println();
				if(gravar)
					gravarArq.println();
			}
		}		
		System.out.println();
		if(gravar)
			gravarArq.println();
	}
	
	
	/**
	 * Conta a quantidade de itens da solu��o.
	 * @return Quantidade de itens da solu��o.
	 */
	private int totalItens(){
		int totalItens = 0;
		int index;
		for(int i = 0; i < this.size(); i++){
			index = this.get(i);
			totalItens += lstConj.get(index).getQtdItens();
		}
		return totalItens;
	}
}
