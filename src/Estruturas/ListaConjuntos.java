package Estruturas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe contendo a lista de conjuntos do arquivo de inst�ncia
 * e a lista de adjac�ncia entre os conjuntos. 
 * 
 * @author Bruno Freitas, Lu�s Felipe
 *
 */
public class ListaConjuntos extends ArrayList<Conjunto>{

	private static final long serialVersionUID = 1L;


	private static String separador = " ";
		
	private int numItens;
	private int numConj;
	
		
	public ListaConjuntos(File arquivo){
		super();
		carregaConjuntos(arquivo);
//		geraListaAdj();
	}
	
	
	/**
	 * Carrega os dados do arquivo de inst�ncias
	 * 
	 * @param arquivo
	 */
	private void carregaConjuntos(File arquivo){
		try {
			BufferedReader buffer = new BufferedReader(new FileReader(arquivo));
			String linha;
			linha = buffer.readLine();

			while(linha != null) {
				String[] conteudo = linha.split(separador);
				if(conteudo[0].compareTo("p") == 0){
					numItens = Integer.parseInt(conteudo[2]);
					numConj = Integer.parseInt(conteudo[3]);					
				}
				else if (conteudo[0].compareTo("s") == 0) {
					Conjunto conj = new Conjunto();
					for(int i = 1; i < conteudo.length; i++){
						conj.addItem(Integer.parseInt(conteudo[i]));
					}
					this.add(conj);
				}

				linha = buffer.readLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Gera a lista de adjac�ncias.
	 */
	public List<List<Integer>> geraListaAdj(){
		List<List<Integer>> lstAdj = new ArrayList<List<Integer>>();
		Conjunto conj;
		
		for(int i = 0; i < this.size(); i++){
			conj = this.get(i);
			List<Integer> lstAux = new ArrayList<Integer>();
			lstAdj.add(lstAux);
			for(int j = 0; j < this.size(); j++){
				if(j != i && conj.comparaCojuntos(this.get(j)) == true){
					conj.incrementaAresta();
					lstAux.add(j);
				}
			}
		}
		return lstAdj;
	}
	
	
	public int qtdItensConjunto(int index){
		return this.get(index).getQtdItens();
	}
	
}
