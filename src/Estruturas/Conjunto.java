package Estruturas;


import java.util.ArrayList;

public class Conjunto extends ArrayList<Integer> {
	
	private static final long serialVersionUID = 1L;

	private int qtdItens;
	private int nroArestas;
	private boolean usado;
	
	public Conjunto() {
		super();
		qtdItens = 0;
		nroArestas = 0;
		usado = false;
	}
	
	
	public void addItem(int item) {
		this.add(item);
		qtdItens++;
	}
	
	
	public void incrementaAresta() {
		this.nroArestas++;
	}
	
	
	public boolean isUsado() {
		return usado;
	}
	
	
	public void setUsado(boolean usado) {
		this.usado = usado;
	}
	
	
	public boolean comparaCojuntos(Conjunto c){
		for(int i = 0; i < c.size(); i++){
			if(this.contains(c.get(i))) return true;
		}
		
		return false;
	}
	
	public int getQtdItens() {
		return qtdItens;
	}
}
