set universe;
set group;
param belongs{group, universe} default 0;

var x{universe} binary;
var y{group} binary;

maximize z: sum{i in group} y[i];

s.t. c1{j in universe}: sum{i in group} y[i]*belongs[i, j] <= 1;

data;

set group := g1 g2 g3 g4 g5;

set universe :=      1 2 3 4 5 6 7 8;

param belongs:  1 2 3 4 5 6 7 8 :=
        g1     	1 . . 1 . . . .
        g2     	. . 1 . 1 1 . .
        g3	. 1 . . . . 1 1
        g4	1 . . 1 1 1 . . 
        g5    	. 1 . 1 . 1 . 1
; 

end;

