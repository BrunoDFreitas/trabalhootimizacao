set universe;
set group;
param belongs{group, universe} default 0;

var x{universe} binary;


maximize z: sum{i in universe} x[i];
#minimize z: sum{i in universe} x[i];

s.t. c1{i in group}: sum{j in universe} x[j]*belongs[i,j] <= 1;


data;

set group := g1 g2 g3 g4 g5 g6;

set universe :=      1 2 3 4 5 6 7 8 9 10;

param belongs:       1 2 3 4 5 6 7 8 9 10 :=
        g1     	1 1 1 1 1 . . . . .
        g2     	. . . . . 1 1 1 1 1
        g3	. 1 1 . . . . 1 1 1
        g4	1 . . . 1 1 1 . . . 
        g5    	. . 1 1 1 1 1 . 1 .
        g6   	1 1 . . . . . 1 . 1
; 

option solver cplex;
solve;

display x;
