#!/usr/bin/python
import sys

try:
    f = open(sys.argv[1], "r")
except:
    sys.exit("Erro na leitura do arquivo de entrada.")

# le primeira linha para o total de conjuntos e max de numeros
line = f.readline()
content = line.split()
numbers = int(content[2])
sets = int(content[3])

# leitura de cada conjunto
numberSet = []
for c in xrange(sets):
	line = f.readline()
	if(line):
		numbersList = line.split()
		del numbersList[0] # deleta elemento "s"
		numbersList = [int(i) for i in numbersList]
		numberSet.append(numbersList)		
f.close()

# gera matriz representando os numeros que fazem parte de cada conjunto
numInSet = []
for c in numberSet:
	present = []
	for var in xrange(1, numbers+1):
		if(var in c):
			present.append(1)
		else:
			present.append(0)
	numInSet.append(present)


# gera arquivo de entrada para o glpk
try:
    out = open(sys.argv[2], "w")
except:
    sys.exit("Erro na geracao do arquivo de saida.")
out.write("data;\n")
out.write("param S := " + str(sets) + ";\n")
out.write("param N := " + str(numbers) + ";\n")
vars_str = ' '.join(str(e) for e in range(1, numbers+1))
out.write("param numberInSet : " + vars_str + " := \n")
for c in range(len(numberSet)):
	out.write('\t' + str(c+1) + '\t' + ' '.join(str(e) for e in numInSet[c]) + '\n')
out.write(";\n")
out.write("end;\n\n")
out.close()

print "Gerado arquivo: ", sys.argv[2]

